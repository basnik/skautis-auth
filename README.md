#README#

Nette extension pro zajištění přihlašování do SkautISu. 
Má několik různých způsobů nastavení - je možné zadat jen číslo jednotky, případně je možné specifikovat výčet funkcí z jednotky, které mají povolený přístup.
Je možné i přímo zadat id uživatele ve SkautISu. Pro ověřování funkcí se používá `key` atribut požadované funkce.
Po přihlášení je uživateli nastavena Nette role `fromSkautis` a také jsou mu nastaveny další role podle jeho funkcí v jednotce.

Obsahuje i dvě předpřipravené komponenty - jedna vygeneruje odkaz do SkautISu na přihlašovací stránku, druhá v případě přihlášeného uživatele zobrazí jeho jméno a kolik zbývá do odhlášení.

##Použité metody ze SkautIS API##

- UserManagement->UserRoleAll
- UserManagement->UserDetail

##Příklad nastavení##

Nastavuje se v `config.neon`:
***
```
skautisAuth:
	allowedUnits: 
		- 12345
		- 34567
		- [22112, ['vedouciStredisko']]
		- [11221, ['hospodarOkres', 'cinovnikOkres']]
	allowedUsers:
		- 1234
	redirectOkLogin: 'Be:Dashboard:'
	redirectBadLogin: 'Fe:Homepage:'
	redirectLogout: 'Fe:Homepage:'
```
***
Tato konfigurace povolí přihlášení komukoli, kdo má nějakou funkci v jednotkách `12345` a `34567`, 
komukoli, kdo má roli `vedouciStredisko` v jednotce `22112` a komukoli, kdo má roli `hospodarOkres` nebo `cinovnikOkres` v jednotce `11221`.
Navíc povolí přihlášení i uživateli, který má ve SkautISu id `1234`.  
Po úspěšném přihlášení bude uživatel přesměrován na `Be:Dashboard:` presenter, akci default. 
Při neúspěšném přihlášení nebo odhlášení bude přesměrován na homepage.