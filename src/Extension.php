<?php
namespace Kiwi\SkautisAuth;

use Nette\Application\IPresenterFactory;
use Nette\Application\Routers\Route;
use Nette\InvalidArgumentException;
use Nette\InvalidStateException;
use Nette\Utils\ArrayList;

/**
 * Sets up the extension, reads and passes its configuration, adds routes.
 */
class Extension extends \Nette\DI\CompilerExtension{
	
	/**
	 * Add our services
	 */
	public function loadConfiguration() {
		
		$config = $this->getConfig();
		$builder = $this->getContainerBuilder();

		$allowedUsers = array_key_exists("allowedUsers", $config) ? $config["allowedUsers"] : null;
		$allowedUnits = array_key_exists("allowedUnits", $config) ? $config["allowedUnits"] : null;
		$redirectOkLogin = array_key_exists("redirectOkLogin", $config) ? $config["redirectOkLogin"] : null;
		$redirectBadLogin = array_key_exists("redirectBadLogin", $config) ? $config["redirectBadLogin"] : null;
		$redirectLogout = array_key_exists("redirectLogout", $config) ? $config["redirectLogout"] : null;

		// either allowed users or allowed units must be specified
		if (($allowedUnits == null && $allowedUsers == null) || (empty($allowedUnits) && empty($allowedUsers))) {
			throw new InvalidArgumentException("SkautisAuth config error: You have to specify either list of allowed units or allowed users.");
		}

		// allowed properties must be both array (if set)
		if (($allowedUsers != null && !is_array($allowedUsers)) || ($allowedUnits != null && !is_array($allowedUnits))) {
			throw new InvalidArgumentException("SkautisAuth config error: allowedUsers and allowedUnits config properties must be array.");
		}

		// check units format (2nd must be array)
		if ($allowedUnits != null) {
			foreach ($allowedUnits as $unit) {
				if (is_array($unit) && count($unit) == 2 && !is_array($unit[1])) {
					throw new InvalidArgumentException("SkautisAuth config error: If you specify roles, they must be defined as array even if there is just one.");
				}
			}
		}

		// todo check redirect configs
		
		// register our services
		$builder->addDefinition($this->prefix('skautisAuthenticator'))->setClass('\Kiwi\SkautisAuth\Authenticator')
			->addSetup('$service->setConfigParams(?, ?, ?, ?, ?)', [$allowedUnits, $allowedUsers, $redirectOkLogin, $redirectBadLogin, $redirectLogout]);

		// define route for confirmation
		$builder->addDefinition($this->prefix('routeSkautisLogin'))
			->setFactory('\Nette\Application\Routers\Route', array('skautisLogin.php<? .*>', 'Kiwi:SkautisAuth:Login:'))
			->setAutowired(FALSE)
			->addTag(\Nette\DI\Extensions\InjectExtension::TAG_INJECT);

		// add our backend route to application router
		$routerService = $builder->getDefinition('router');
		$routerService->addSetup('$service->prepend(?)', [$this->prefix('@routeSkautisLogin')]);
	}

	public function afterCompile(\Nette\PhpGenerator\ClassType $class) {
		$initialize = $class->getMethod('initialize');
		$initialize->addBody('$this->getByType("\Kiwi\SkautisAuth\Authenticator", true)->verifyLogin();');
	}
}
