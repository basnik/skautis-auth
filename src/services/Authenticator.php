<?php

namespace Kiwi\SkautisAuth;

use Kiwi\Entities\User;
use Kiwi\Services\SystemService;
use Kiwi\Services\UserService;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;

/**
 * Skautis user authenticator. Handles login and serves as transport container for redirect configuration.
 */
class Authenticator {

	use \Nette\SmartObject;

	const SKAUTIS_ROLE_IDENT = 'fromSkautis';

	const USER_DATA_KEY = 'skautisId';

	const SKAUTIS_LOGOUT_KEY = 'skautIS_Logout';

	/** @var \Nette\Security\User */
	protected $user;
	
	/** @var \Skautis\Skautis */
	protected $skautis;
	
	/** @var \Nette\Http\Request */
	protected $httpRequest;

	/** @var \Nette\Http\Response */
	protected $httpResponse;

	/** @var Nette\Application\LinkGenerator */
	protected $linkGenerator;
	
	/** @var array of ids */
	protected $allowedUnits;

	/** @var array of ids */
	protected $allowedUsers;

	/** @var string Target presenter and action to redirect after successfull login */
	protected $redirectOkLogin;

	/** @var string Target presenter and action to redirect after unsuccessfull login */
	protected $redirectBadLogin;

	/** @var string Target presenter and action to redirect after logout */
	protected $redirectLogout;

	/** @var UserService Kiwi user service */
	protected $userService;

	/** @var SystemService */
	protected $system;


	/**
	 * Constructor, used for DI, do not call directly.
	 * @Internal
	 */
	public function __construct(\Nette\Security\User $user, \Skautis\Skautis $skautis, \Nette\Http\Request $httpRequest, SystemService $system,
	                            \Nette\Http\Response $httpResponse, \Nette\Application\LinkGenerator $linkGen, UserService $userService) {
		$this->user = $user;
		$this->skautis = $skautis;
		$this->httpRequest = $httpRequest;
		$this->system = $system;
		$this->httpResponse = $httpResponse;
		$this->linkGenerator = $linkGen;
		$this->userService = $userService;
	}

	/**
	 * DI, sets configuration parameters during service creation. Used for DI, do not call directly.
	 * @Internal
	 */
	public function setConfigParams($allowedUnits, $allowedUsers, $redirectOkLogin, $redirectBadLogin, $redirectLogout) {
		$this->allowedUnits = $allowedUnits;
		$this->allowedUsers = $allowedUsers;
		$this->redirectOkLogin = $redirectOkLogin;
		$this->redirectBadLogin = $redirectBadLogin;
		$this->redirectLogout = $redirectLogout;
	}


	/**
	 * Returns Presenter:action set in config for successfull login redirect.
	 * @return string
	 */
	public function getRedirectForOkLogin() {
		return $this->redirectOkLogin;
	}


	/**
	 * Returns Presenter:action set in config for unsuccessfull login redirect.
	 * @return string
	 */
	public function getRedirectForBadLogin() {
		return $this->redirectBadLogin;
	}


	/**
	 * Checks if user is logged in and is from SkautIS.
	 * If yes, prolongs his session in SkautIS, otherwise logs user out.
	 */
	public function verifyLogin() {

		// do not verify if this is logout request (handled in LoginPresenter)
		if ($this->isLogoutRequest()) {
			return;
		}

		try {
			// check if user is logged in and is from skautis
			if ($this->user->isLoggedIn() && $this->user->isInRole(self::SKAUTIS_ROLE_IDENT)) {
				// if yes, check if he is logged in in skautis - if yes, update login time, if not, log out
				// this is hard check, so login time is also updated using this call
				if (!$this->skautis->getUser()->isLoggedIn(true)) {
					$this->logout();
				}
			}
		} catch (\Exception $e) {
			\Tracy\Debugger::log($e);
			$this->logout();
		}
	}

	/**
	 * Performs an authentication. On bad login throws exception.
	 * No parameters needed as target service accepts whole post array.
	 * 
	 * @return void
	 * @throws Nette\Security\AuthenticationException
	 */
	public function login() {

		try{
			// first try to login the user
			$this->skautis->setLoginData($this->httpRequest->getPost());

			// fetch user data
			$skautisUserData = $this->skautis->user->UserDetail();
			if (!$skautisUserData->IsActive || !$skautisUserData->IsEnabled || !$skautisUserData->HasMembership) {
				throw new \Nette\Security\AuthenticationException('User inactive or without active membership.', IAuthenticator::NOT_APPROVED);
			}

			// fetch user roles in skautis
			$skautisRoles = $this->skautis->UserManagement->UserRoleAll([
				"ID_User" => $skautisUserData->ID
			]);

			$userRolesKeys = [];
			// now go through allowed users - if user is among them, we can proceed
			if (is_array($this->allowedUsers)) {
				foreach ($this->allowedUsers as $userId) {
					if ($skautisUserData->ID == $userId) {
						$userRolesKeys = $this->getAllActiveRolesWithKey($skautisRoles);
						break;
					}
				}
			}

			// now go through allowed units - if user has role for any of them (and if the role matches allowed role list (if any)), we can proceed
			if (is_array($this->allowedUnits)) {
				foreach ($this->allowedUnits as $unit) {
					$rolesFound = [];
					if (is_array($unit)) {
						// first element contains unit id, second is list of allowed roles
						$userRolesKeys = array_merge($userRolesKeys, $this->getRolesFor($skautisRoles, $unit[0], $unit[1]));
					} else {
						// all roles for this unit are allowed and unit is its id
						$userRolesKeys = array_merge($userRolesKeys, $this->getRolesFor($skautisRoles, $unit));
					}
				}
			}

			// if user cannot proceed, return false as he is not allowed to log in
			if (count($userRolesKeys) == 0) {
				// no user roles - unallowed to login
				throw new \Nette\Security\AuthenticationException("User has no roles to use. User roles from skautis: [".print_r($skautisRoles, true)."].", IAuthenticator::IDENTITY_NOT_FOUND);
			}

			// check if user with this skautis id exists in database - if not, create it
			$kiwiUsersFound = $this->userService->getAllMatching([["JSON_VALID(data) AND JSON_CONTAINS(data, %s, '$.skautisId')", $skautisUserData->ID]]);
			if (count($kiwiUsersFound) > 0) {
				$kiwiUser = $kiwiUsersFound[0];
			} else {
				$kiwiUser = new User();
				$kiwiUser->name = $skautisUserData->Person;
				$kiwiUser->email = $skautisUserData->ID."@skautis"; // email must be unique
				$kiwiUser->data = [self::USER_DATA_KEY => $skautisUserData->ID];
				$this->userService->save($kiwiUser);
			}

			// this role is to allow fast and easy detection of skautis-logged user in target application
			$userRolesKeys[] = self::SKAUTIS_ROLE_IDENT;

			// we can log in, so create the entity object and place all keys from user roles in it
			$this->user->login(new Identity($kiwiUser->id, $userRolesKeys, [
				'skautisId' => $skautisUserData->ID,
				'skautisPersonId' => $skautisUserData->ID_Person,
				'name' => $skautisUserData->Person
			]));

			$this->system->logActivity(sprintf("Uživatel %s (%s, %d) přihlášen přes SkautIS.", $kiwiUser->email, $kiwiUser->name, $kiwiUser->id));

		} catch (\Skautis\Exception $e){
			throw new \Nette\Security\AuthenticationException('Problems with communication with Skautis', IAuthenticator::FAILURE, $e);
		}
	}

	/**
	 * Logs user out and redirects to desired "after logout" link specified in config.
	 * After redirect, stops the execution of the script.
	 */
	public function logout() {
		$this->user->logout(true);
		// skautis library still thinks we are logged in, which may cause some problems when comunicating with skautis, so we need to erase login data
		$this->skautis->getUser()->resetLoginData();
		$this->httpResponse->redirect($this->linkGenerator->link($this->redirectLogout));
		exit;
	}

	/**
	 * Checks if current request is skautis logout request.
	 * Logout request is POST and has proper logout key set.
	 */
	public function isLogoutRequest() {
		$skautisLogout = $this->httpRequest->getPost(self::SKAUTIS_LOGOUT_KEY);
		return $skautisLogout != NULL;
	}


	/**
	 * Gets all roles (their keys) from roles array for given unit and given role key list. Role must be active and have the Key attribute to be picked.
	 * @Internal
	 */
	private function getRolesFor($roles, $unitId, $allowedRoleKeys = []) {
		$userRoles = [];
		foreach ($roles as $roleData) {
			if ($roleData->IsActive == true && isset($roleData->Key) && $roleData->ID_Unit == $unitId
				&& ((count($allowedRoleKeys) > 0 && in_array($roleData->Key, $allowedRoleKeys)) || count($allowedRoleKeys) == 0)) {
				$userRoles[] = $roleData->Key;
			}
		}
		return array_unique($userRoles);
	}


	/**
	 * Gets all roles (their keys) from roles array. Role must be active and have the Key attribute to be picked.
	 * @Internal
	 */
	private function getAllActiveRolesWithKey($userRoles) {
		$roleKeys = [];
		foreach ($userRoles as $roleData) {
			if ($roleData->IsActive && isset($roleData->Key)) {
				$roleKeys[] = $roleData->Key;
			}
		}
		return array_unique($roleKeys);
	}
}
