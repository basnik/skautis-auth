<?php

namespace Kiwi\SkautisAuth;

/**
 * Used for DI to create and autowire dependencies.
 */
interface ILoginLinkControlFactory {
	
	/**
	 * @return LoginLinkControl
	 */
	public function create();
}
