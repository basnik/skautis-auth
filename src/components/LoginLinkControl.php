<?php
namespace Kiwi\SkautisAuth;

use Nette\Application\UI\Form;
use Nette\Utils\DateTime;
use Skautis\Skautis;

/**
 *
 */
class LoginLinkControl extends \Nette\Application\UI\Control {


	/** @var Skautis */
	protected $skautis;

	/**
	 * Contructor used for DI, do not call directly.
	 * @Internal
	 */
	public function __construct(\Skautis\Skautis $skautis, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL) {
		$this->skautis = $skautis;
	}
	
	public function render(){
		echo $this->skautis->getLoginUrl();
	}
}
