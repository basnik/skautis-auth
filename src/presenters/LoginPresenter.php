<?php
namespace Kiwi\SkautisAuth;

/**
 *
 * @author basnik
 */
class LoginPresenter extends \Nette\Application\UI\Presenter{
	
	/** @inject @var \Kiwi\SkautisAuth\Authenticator */
	public $authenticator;
	
	public function actionDefault(){

		// if this is logout request, user was already logged out of SkautIS - log user out - that should also redirect to desired places
		if ($this->authenticator->isLogoutRequest()) {
			$this->authenticator->logout();
		}

		// k loginu se pouziva post pole, ouje ...
		try {
			$result = $this->authenticator->login();
		} catch (\Nette\Security\AuthenticationException $e) {
			\Tracy\Debugger::log($e, \Tracy\Debugger::EXCEPTION);
			$this->flashMessage('Váš účet ve SkautISu je neaktivní nebo není oprávněn k přihlášení na tomto webu. Mohlo také dojít k chybě či výpadku na straně SkautISu.', 'error');
			$result = FALSE;
		}


		if($result === FALSE){
			$this->flashMessage('Přihlášení nebylo úspěšné.', 'error');
			$this->redirect(":".$this->authenticator->getRedirectForBadLogin());
		}else{
			$this->redirect(":".$this->authenticator->getRedirectForOkLogin());
		}
	}
}
