<?php
namespace Kiwi\SkautisAuth;

use Nette\Application\UI\Form;
use Nette\InvalidStateException;
use Nette\Utils\DateTime;
use Skautis\Skautis;

/**
 *
 */
class LoginStatusControl extends \Nette\Application\UI\Control {

	/** @var \Nette\Security\User */
	protected $user;

	/** @var Skautis */
	protected $skautis;

	/** @var Authenticator */
	protected $authenticator;

	/**
	 * Contructor used for DI, do not call directly.
	 * @Internal
	 */
	public function __construct(\Nette\Security\User $user, \Skautis\Skautis $skautis, \Kiwi\SkautisAuth\Authenticator $authenticator, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL) {
		$this->user = $user;
		$this->skautis = $skautis;
		$this->authenticator = $authenticator;
	}
	
	public function render(){

		// user must be from skautis to be able to display this component
		if(!$this->user->isInRole("fromSkautis")) {
			throw new InvalidStateException("Trying to display Skautis status control with user not from Skautis.");
		}

		$logout = $this->skautis->getUser()->getLogoutDate();
		$now = new DateTime();
		$remaining = $logout->diff($now);

		$userIdentity = $this->user->getIdentity();
		$this->template->skautisUserName = $userIdentity->name;
		$this->template->skautisLogoutUrl = $this->skautis->getLogoutUrl();
		$this->template->skautisLogoutMins = $remaining->i;

		$this->template->setFile(__DIR__.'/../templates/components/loginStatus.latte');
		$this->template->render();
	}
}
