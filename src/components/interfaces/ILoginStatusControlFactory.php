<?php

namespace Kiwi\SkautisAuth;

/**
 * Used for DI to create and autowire dependencies.
 */
interface ILoginStatusControlFactory {
	
	/**
	 * @return LoginStatusControl
	 */
	public function create();
}
